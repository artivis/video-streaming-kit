
Quick Start Guide
=================

<!-- ![Assembled kit](img/kit_assembled.jpg) -->

This guide will help you assemble your ARTiVIS DIY Video Streaming Kit and start streaming with its default configuration. It will also give you some pointers for more advanced configuration of the device.

If you are trying to make your own version of the kit we present here, which we use for workshops and development,  we recommend you to start from the [System Architecture](architecture.md) document.

If you find anything missing from this guide please see the [CONTRIBUTING](./CONTRIBUTING.md) file.

What you will need
------------------

for the basic out-of-the-box functionality you will need:

 * the kit's hardware components (see the next section for a list)
 * a personal computer with Wifi networking and a USB port
 * a Web browser like [Chromium](https://www.chromium.org/getting-involved/download-chromium) or [Firefox](http://getfirefox.com)
 * a flexible Media Player that can play all kinds of streaming video, like [VLC](http://videolan.org)

for more advanced configuration you will need:

 * a Secure Shell (SSH) client like [OpenSSH](https://www.openssh.com) or [PuTTy](https://www.chiark.greenend.org.uk/~sgtatham/putty/)
 * a Serial Terminal program like [CuteCom](https://gitlab.com/cutecom/cutecom) or PuTTy.

What's in the box?
------------------

### Raspberry Pi Zero W

![Raspberry Pi Zero W](img/kit_raspberrypi.jpg)

The [Raspberry Pi Zero W](https://www.raspberrypi.org/products/raspberry-pi-zero-w/) is the heart of the kit. It's a small, low-cost and low-power open hardware computer that runs the [Raspbian GNU/Linux](https://raspbian.org) operating system and [our custom embedded streaming server software](http://gitlab.com/artivis/streamd/).

### Raspberry Pi Zero Camera Module

![Raspberry Pi Zero Camera Module](img/kit_camera.jpg)

This camera module is a smaller version of the standard [Raspberry Pi Camera Module](https://www.raspberrypi.org/documentation/hardware/camera/README.md) made specifically for the Zero form factor. It is a small, low-cost cmara module that makes use of the onboard high-speed MIPI-CSI interface to grab still images up to 2592x1944 pixels and video up to 1080p (1920x1080) at 30 frames per second.

You can also use standard Raspberry Pi camera modules with the Zero W but you will need a cable adapter to match the smaller pitch of the Zero W's camera connector.

### Micro SD card and SD card adapter

![Micro SD card and SD card adapter](img/kit_sdcard.jpg)

This is just a standard micro SD card with an adapter that makes it easier to connect it to a standard SD card reader. If you plan to record the video you are capturing, the card should be at least a class 10.

The card should have been provided to you already with our custom Raspbian operating system image recorded onto it. This image includes all the necessary software and configurations to stream video out of the box.

If you would like to customise the operating system image or build one from scratch, you can use the same [sdbuild tool](http://gitlab.com/artivis/sdbuild/) we use to create our own development images.

### 5V USB Power Bank

![5V USB Power Bank](img/kit_powerbank.jpg)

This is a standard 5V USB power bank with a capacity of 2000mAh. In our tests it was able to power an encrypted live video stream to a [ZeroTier](http://zerotier.com) endpoint for about 6 hours.

Instead of this power bank you can use any other 5V micro USB source like a mains adapter, another power bank or even your own computer.

**Please note:** the USB cable provided with the power bank is power-only. This means that it will only work for providing power to the Raspberry Pi or for charging the power bank and not for communication between the Raspberry Pi and your computer.

### Micro USB cable

![Micro USB cable](img/kit_cable.jpg)

A generic micro USB cable for connecting the computer to the Raspberry Pi.

### Misc. adapters

![Misc. adapters](img/kit_misc_adapters.jpg)

A set of adapters that allow for connecting the Raspberry Pi Zero to other devices like USB peripherals, HDMI screens and generic Raspberry Pi GPIO extension modules like [HATs](https://www.raspberrypi.org/blog/introducing-raspberry-pi-hats/)

Kit assembly
------------

### Attach camera module to Raspberry Pi

![Attach camera module to Raspberry Pi](img/assembly_camera.jpg)

The first step is to connect the camera module to the Raspberry Pi. To do so, you should first locate the camera port on one of the edges of the board (use the picture above as reference) and slightly pull on both edges of the black retainer clip away from the board (don't pull too hard, the clip is fragile and is not meant to come off the board). Then inser the camera module's ribbon cable on the camera port as shown above and carefully return the black clip to its original position.

Optionally, you can peel off the protective paper from the adhesive tabs on the back of the camera module and fold it over the back of the Raspberry Pi, making for a more compact assembly:

![Optionally, fold the camera cable over the back of the camera module](img/assembly_foldcamera.jpg)

### Insert the micro SD card in the Raspberry Pi

![Insert the micro SD card in the Raspberry Pi](img/assembly_sdcard.jpg)

Insert the micro SD card into the micro SD card slot of the Raspberry Pi Zero W, as shown above.

### Attach the Power Bank

![Attach the Power Bank](img/assembly_powerup.jpg)

Connect the power bank's micro USB plug to the micro USB port labeled `PWR` on the board. A green light should flicker for a few seconds and eventually turn to a solid green.

Device Setup
------------

An automated first time configuration script is executed when the device starts for the first time from a new SD card, so you should wait for a few minutes after first boot before starting to use it.

When the device starts it creates a Wifi network with the name `artivis-xyzw` where `xyzw` is a unique device identifier (this is so that when there are many kits in one room, their network and host names are not the same by default).

You can connect to the network with the password `artivisdiy`. The device has a static address of `192.168.100.1` and should automatically start streaming an `mjpeg` video on port `3000`. To check, open the VLC media player, select the option "Media -> Open Network Stream", set the URL to `tcp/mjpeg://192.168.100.1:3000` and press "Play" to view the stream.

Next Steps
----------

So now you have a working video streaming node running with a default configuration, and you would like to start modifying it to fit your needs. Here are a couple of things you might want to do:

### Get a command line shell on the device

A [command-line shell](https://en.wikipedia.org/wiki/Command-line_interface) is currently the preferred way to configure the device. There are two different ways to access the shell on your Raspberry Pi:

 * connect to the device via `SSH` using a client like OpenSSH or PuTTy. The default address is `ssh://artivis-xyzw.local` where `artivis-xyzw` is the actual hostname of the device.

 * connect via a serial terminal with a serial terminal app like CuteCom or PuTTy. To do this, connect the micro usb cable to your computer and to the `USB` port on the Raspberry Pi and connect to the new serial port that just appeared (the way to do this will vary according to your operating system, see [here for an example](https://nl.mathworks.com/help/supportpkg/arduinoio/ug/find-arduino-port-on-windows-mac-and-linux.html)).

In both instances the terminal will display a prompt asking for your login and password ( both are `artivis` by default, and you should change this, [see below](#Secure_the_device))

Once logged in, you should take some time familiarising yourself with the [UNIX command line](https://command-line-tutorial.readthedocs.io/), with the [GNU nano text editor](https://www.nano-editor.org) and with basic [Bash](http://tldp.org/LDP/Bash-Beginners-Guide/html/index.html) scripting.

### Connect to your own network

The most simple way to configure your wifi networking is to use the [standard Debian network interface files](https://wiki.debian.org/WiFi/HowToUse#Command_Line).

In alternative you could setup [Wicd](https://launchpad.net/wicd), [Conman](https://01.org/connman), [Network Manager](https://wiki.debian.org/NetworkManager) or [Systemd-Networkd](https://wiki.debian.org/SystemdNetworkd) (these are alternative ways to setup networking on Linux-based systems)

### Configure the streaming server

To configure the streaming server you can edit the file `/etc/streamd.conf` and then restart the server with `systemctl restart streamd`. Please refer to the [`streamd`](https://gitlab.com/artivis/streamd) documentation to understand the structure and syntax of the configuration file.

### Secure the device

By default the device is minimally secured against intrusion by outsiders by creting its own wifi network for the purpose of configuration and experimenting. However, because both the devices login/password and network SSID/password combinations are known (i.e. its printed on this quickstart guide and embedded in the open-source image creation scripts) care should be taken before connecting the device to the outside world. Here are some pointers on how to do this:

 * edit the `/etc/hostapd/hostapd.conf` file and change the SSID and password of the self-created wifi network.
 * change the default login password using the `passwd` command
 * [disable password-based login via `SSH`](https://wiki.debian.org/SSH#Securing)
 * set up a firewall using [nfttables](https://wiki.debian.org/nftables) or [firewalld](https://firewalld.org/documentation/configuration/)
